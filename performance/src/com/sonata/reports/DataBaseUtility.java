package com.sonata.reports;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DataBaseUtility {
	private static final String driverName = "oracle.jdbc.driver.OracleDriver";

	private static final String dbUrl = "jdbc:oracle:thin:@localhost:1521";

	private static final String dbUserName = "system";

	private static final String dbPassword = "Software123";

	private static final String dbName = "perftest";

	private static Connection connection;

	private static Statement statement;

	private static ResultSet resultSet;

	public final static synchronized void createConnection() {
		try {
			Class.forName(driverName);
			connection = DriverManager.getConnection(dbUrl + ":" + dbName,
					dbUserName, dbPassword);
			statement = connection.createStatement();
			System.out.println("DB connection established successfully");
		} catch (ClassNotFoundException e) {
			System.out.println("Exception in createConnection: " + e);
		} catch (SQLException e) {
			System.out.println("Exception in createConnection: " + e);
		} catch (Exception e) {
			System.out.println("Exception in createConnection: " + e);
		}
	}

	public final static synchronized int fetchingRecordBasedOnDate(
			String sampler, String brndName, String runDate) {
		int count = 0;
		try {
			String sqlquery = "select * from performance where (sampler='"
					+ sampler + "' and brandname='" + brndName
					+ "') and executiondate='" + runDate + "'";
			resultSet = statement.executeQuery(sqlquery);
			while (resultSet.next()) {
				return resultSet.getInt(1);
			}
			//System.out.println("Resulset set object created successfully");
		} catch (SQLException e) {
			count = -1;
			System.out.println("Exception in fetchSingleRecord: " + e);
		} catch (Exception e) {
			count = -1;
			System.out.println("Exception in fetchSingleRecord: " + e);
		}
		return count;
	}

	public final static synchronized int fetchSingleRecord() {
		int count = 0;
		try {
			String sqlquery = "select max(samplerindex) from performance order by samplerindex desc";
			resultSet = statement.executeQuery(sqlquery);

			/*
			 * if (!resultSet.next()){ System.out.println("enetr"); return
			 * count; }
			 */
			while (resultSet.next()) {
				return resultSet.getInt(1);
			}
			System.out.println("Resulset set object created successfully");
		} catch (SQLException e) {
			count = -1;
			System.out.println("Exception in fetchSingleRecord: " + e);
		} catch (Exception e) {
			count = -1;
			System.out.println("Exception in fetchSingleRecord: " + e);
		}
		return count;
	}

	public final static synchronized ResultSet executeQuery(String sqlquery) {
		try {
			resultSet = statement.executeQuery(sqlquery);
			/*
			 * while (resultSet.next()) { logger1.info(resultSet.getInt(1) +
			 * "  " + resultSet.getString(2) + "  " + resultSet.getString(3)); }
			 */
			System.out.println("Resulset set object created successfully");
		} catch (SQLException e) {
			System.out.println(e);
		} catch (Exception e) {
			System.out.println(e);
		}
		return resultSet;
	}

	public final static synchronized void insertDataToDb(int samplerIndex,
			String brandName, String date, String sampler, int avg, int max,
			int min, int nintyper, float error) {
		try {
			String query = "INSERT INTO performance VALUES (?,?,?,?,?,?,?,?,?)";
			PreparedStatement statement = connection.prepareStatement(query);
			/*
			 * String sql = "Insert into performance"+ "values(" + brandName +
			 * "," + "to_timestamp(" + date+"," + "'dd-MM-yyyy HH24:MI')" + ","
			 * + sampler + "," + avg + "," + max + "," + min + "," + nintyper +
			 * "," + error + ")";
			 */
			// statement.executeUpdate(sql);

			SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yy HH.mm.ss");
			Date dateVal = fmt.parse(date);
			java.sql.Timestamp timstamp = new java.sql.Timestamp(
					dateVal.getTime());
			statement.setInt(1, samplerIndex);
			statement.setString(2, brandName);
			statement.setTimestamp(3, timstamp);
			statement.setString(4, sampler);
			statement.setInt(5, avg);
			statement.setInt(6, max);
			statement.setInt(7, min);
			statement.setInt(8, nintyper);
			statement.setFloat(9, error);
			statement.execute();

			System.out.println("records inserted to DB");
		} catch (Exception e) {
			System.out.println("Exception in insertDataToDb: " + e);
		}
	}

	public static synchronized void disconnect() {
		try {
			if (statement != null)
				statement.close();
			if (resultSet != null)
				resultSet.close();
			if (connection != null)
				connection.close();
		} catch (SQLException e) {
			System.out.println("Exception in disconnect:" + e);
		}
	}
}
