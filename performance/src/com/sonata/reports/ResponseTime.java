package com.sonata.reports;

import java.util.List;

public class ResponseTime
{
   private List<Integer> reponseList;

   private List<String> errorList;

   private int labelCounter;
   private String brandName;
   private String executionDate;
   private String samplerName;
   private int average;
   private int max;
   private int min;
   private int nintyPer;
   private float error;

   public void setResponseList(List<Integer> responseList)
   {
      this.reponseList = responseList;
   }

   public List<Integer> getResponseList()
   {
      return reponseList;
   }

   public void setErrorList(List<String> errorList2)
   {
      this.errorList = errorList2;
   }

   public List<String> getErrorList()
   {
      return errorList;
   }

   public void setRepeatationCounter(int labelCounter)
   {
      this.labelCounter = labelCounter;
   }

   public int getRepeatationCounter()
   {
      return labelCounter;
   }
   
   public void setBrandName(String brandName)
   {
      this.brandName = brandName;
   }

   public String getBrandName()
   {
      return brandName;
   }
   
   public void setExecutionDate(String executionDate)
   {
      this.executionDate = executionDate;
   }

   public String getExecutionDate()
   {
      return executionDate;
   }
   
   public void setSamplerName(String samplerName)
   {
      this.samplerName = samplerName;
   }

   public String getSamplerName()
   {
      return samplerName;
   }
   
   public void setAverage(int average)
   {
      this.average = average;
   }

   public int getAverage()
   {
      return average;
   }
   
   public void setMax(int max)
   {
      this.max = max;
   }

   public int getMax()
   {
      return max;
   }
   
   public void setMin(int min)
   {
      this.min = min;
   }

   public int getMin()
   {
      return min;
   }
   
   public void setNintyPer(int nintyPer)
   {
      this.nintyPer = nintyPer;
   }

   public int getNintyPer()
   {
      return nintyPer;
   }
   
   public void setErrorRate(float error)
   {
      this.error = error;
   }

   public float getErrorRate()
   {
      return error;
   }

}
