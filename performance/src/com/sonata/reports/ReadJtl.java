package com.sonata.reports;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import static java.lang.Math.round;
import au.com.bytecode.opencsv.CSVReader;

public class ReadJtl {

	/**
	 * <P>
	 * The method is used to read the given JTL file and store the sampler name
	 * and its values
	 * </p>
	 * 
	 * @param jtlFileName
	 * @return
	 */
	private final ConcurrentHashMap<String, ResponseTime> readJtlFile(
			String jtlFileName) {
		ConcurrentHashMap<String, ResponseTime> jtlDataMap = null;
		CSVReader csvReader = null;
		File jtlFileObject = new File(jtlFileName);
		if (jtlFileObject.exists() && jtlFileObject.getName().endsWith(".jtl")) {
			try {
				csvReader = new CSVReader(new FileReader(jtlFileName));
				List jtlDataList = csvReader.readAll();
				jtlDataMap = new ConcurrentHashMap<String, ResponseTime>();
				for (int i = 0; i < jtlDataList.size(); i++) {
					String[] jtlDatArray = (String[]) jtlDataList.get(i);
					ResponseTime resObject = new ResponseTime();
					List<Integer> responseList = new ArrayList<Integer>();
					List<String> errorList = new ArrayList<String>();
					int counter = 0;
					try {
						responseList.add(Integer.parseInt(jtlDatArray[1]));
						errorList.add(jtlDatArray[4]);

						if (!jtlDataMap.containsKey(jtlDatArray[2])) {
							counter++;

							resObject.setResponseList(responseList);
							resObject.setErrorList(errorList);
							resObject.setRepeatationCounter(counter);

							jtlDataMap.put(jtlDatArray[2], resObject);
						} else {
							ResponseTime storedDataObject = jtlDataMap
									.get(jtlDatArray[2]);
							int storedCounter = storedDataObject
									.getRepeatationCounter();
							storedCounter = storedCounter + 1;

							List<Integer> storedResList = storedDataObject
									.getResponseList();
							storedResList.addAll(responseList);

							List<String> storedErrList = storedDataObject
									.getErrorList();
							storedErrList.addAll(errorList);

							resObject.setResponseList(storedResList);
							resObject.setErrorList(storedErrList);
							resObject.setRepeatationCounter(storedCounter);

							jtlDataMap.put(jtlDatArray[2], resObject);

						}
					} catch (Exception e) {
					}
				}
			} catch (FileNotFoundException e) {
				System.out.println("Exception in readJtlFile: " + e);
			} catch (IOException e) {
				System.out.println("Exception in readJtlFile: " + e);
			} catch (Exception e) {
				System.out.println("Exception in readJtlFile: " + e);
			} finally {
				if (csvReader != null) {
					try {
						csvReader.close();
					} catch (IOException e) {
						System.out.println(e);
					}
				}
			}
		}
		return jtlDataMap;
	}

	/**
	 * <p>
	 * The methods is used to read the stored value and calculate the analysis
	 * for JTL file
	 * </p>
	 * 
	 * @param storedMap
	 * @param executionDate
	 * @return
	 */
	private final List<ResponseTime> calculate_Analysis_Value(
			ConcurrentHashMap<String, ResponseTime> storedMap,
			String executionDate, String brandName) {
		List<ResponseTime> finalAnalysisList = new ArrayList<ResponseTime>();
		for (Entry<String, ResponseTime> storedEntry : storedMap.entrySet()) {
			try {
				ResponseTime responseObject = new ResponseTime();
				int min, max, occurenceOfSample, sum = 0, avg, failCount = 0, nintyPer;
				String labelName = storedEntry.getKey().trim();
				ResponseTime resObject = storedEntry.getValue();
				List<Integer> resList = resObject.getResponseList();
				List<String> errorList = resObject.getErrorList();
				Collections.sort(resList);
				min = resList.get(0);
				max = resList.get(resList.size() - 1);
				occurenceOfSample = resObject.getRepeatationCounter();
				for (Integer resTime : resList) {
					sum += resTime;
				}

				for (String errList : errorList) {
					try {
						String val = errList.substring(
								errList.lastIndexOf(":") + 1).trim();
						if (val.matches("\\d+") && !val.matches("0")) {
							failCount++;
						}
					} catch (Exception e) {
						System.out.println(e);
					}
				}
				avg = round(sum / occurenceOfSample);
				float failPer = (failCount * 100) / occurenceOfSample;
				int index = (int) round(0.9 * (occurenceOfSample - 1));
				nintyPer = resList.get(index);
				responseObject.setBrandName(brandName);
				responseObject.setExecutionDate(executionDate);
				responseObject.setSamplerName(labelName);
				responseObject.setAverage(avg);
				responseObject.setMax(max);
				responseObject.setMin(min);
				responseObject.setNintyPer(nintyPer);
				responseObject.setErrorRate(failPer);
				finalAnalysisList.add(responseObject);
			} catch (Exception e) {
				System.out.println("Exception in calculate_Analysis_Value: "
						+ e);
			}
		}
		return finalAnalysisList;
	}

	private void writeDataToDb(List<ResponseTime> analysisList) {
		try {
			DataBaseUtility.createConnection();
			int samplerIndex = DataBaseUtility.fetchSingleRecord();
			if (samplerIndex >= 0) {
				for (ResponseTime resObject : analysisList) {
					samplerIndex++;
					String brandName = resObject.getBrandName().trim();
					String execDate = resObject.getExecutionDate().trim();
					String samplerName = resObject.getSamplerName().trim();
					int average = resObject.getAverage();
					int max = resObject.getMax();
					int min = resObject.getMin();
					int nintyPer = resObject.getNintyPer();
					float error = resObject.getErrorRate();
					int record = DataBaseUtility.fetchingRecordBasedOnDate(
							samplerName, brandName, execDate);
					if (record < 0 || record == 0) {
						DataBaseUtility.insertDataToDb(samplerIndex, brandName,
								execDate, samplerName, average, max, min,
								nintyPer, error);
					} else {
						System.out.println("Record already exist by this date:"
								+ brandName + "\t" + samplerName + "\t"
								+ execDate);
					}
				}
			}
			DataBaseUtility.disconnect();
		} catch (Exception e) {
			System.out.println("Exception in writeDataToDb: " + e);
		}
	}

	/**
	 * <p>
	 * The method is used to read the JTL folder and store the JTL File name in
	 * LIST
	 * </p>
	 * 
	 * @param jtlFlder
	 * @return
	 */
	private List<File> readFolderAndStoreJtlFiles(File jtlFlder) {
		List<File> fileList = new ArrayList<File>();
		SimpleDateFormat fmt = new SimpleDateFormat();
		File[] files = jtlFlder.listFiles();
		int counter = 0;
		for (File fileName : files) {
			if (fileName.getName().endsWith(".jtl")) {
				if (counter == 0)
					fileList.add(fileName);

				if (counter != 0) {
					Iterator<File> iterator = fileList.iterator();
					boolean fileFound = true;
					while (iterator.hasNext()) {
						String strdFileName = iterator.next().getName();
						String currFileName = fileName.getName();

						try {
							String strdName = strdFileName.substring(0,
									strdFileName.lastIndexOf("_"));
							String currName = currFileName.substring(0,
									currFileName.lastIndexOf("_"));
							if (strdName.equalsIgnoreCase(currName)) {
								String strtString = strdFileName.substring(
										strdFileName.lastIndexOf("_") + 1)
										.replace(".jtl", "");
								String currString = currFileName.substring(
										currFileName.lastIndexOf("_") + 1)
										.replace(".jtl", "");
								fmt.applyPattern("yyyyMMdd-HHmmss");

								Date strtDate = fmt.parse(strtString);
								Date currDate = fmt.parse(currString);
								if (currDate.getTime() > strtDate.getTime()) {
									iterator.remove();
									fileList.add(fileName);
									fileFound = false;
								}
								break;
							}
						} catch (Exception e) {
							System.out.println("File is not in proper format:"
									+ fileName);
						}
					}
					if (fileFound)
						fileList.add(fileName);
				}
				counter++;
			}
		}
		return fileList;
	}

	/*
	 * public static void main(String[] args) { String folderName =
	 * "C:\\MyWork\\apache-jmeter-3.1\\bin\\C4C_Perf\\results\\"; String
	 * brandName = "C4C";
	 */

	public final void storeJtlResultsToDB(String folderName, String brandName) {

		String perfFolderName = "data-load-test-";

		Date currentDate = new Date();
		SimpleDateFormat fmt = new SimpleDateFormat();
		fmt.applyPattern("YYYYMMdd");

		String flderDate = fmt.format(currentDate);
		perfFolderName = perfFolderName + flderDate;
		perfFolderName = folderName + perfFolderName;

		File jtlFlder = new File(perfFolderName);
		if (jtlFlder.exists()) {

			ReadJtl r = new ReadJtl();
			List<File> jtlFilesList = r.readFolderAndStoreJtlFiles(jtlFlder);
			for (File jtlFile : jtlFilesList) {
				try {
					String name = jtlFile.getName().replace(".jtl", "").trim();
					String dateString = name
							.substring(name.lastIndexOf("_") + 1);

					fmt.applyPattern("yyyyMMdd-HHmmss");
					Date dateVal = fmt.parse(dateString);

					fmt.applyPattern("dd-MMM-yy HH.mm.ss");
					String runDate = fmt.format(dateVal);

					ConcurrentHashMap<String, ResponseTime> storedMap = r
							.readJtlFile(jtlFile.toString());
					List<ResponseTime> analysisList = r
							.calculate_Analysis_Value(storedMap, runDate,
									brandName);
					r.writeDataToDb(analysisList);

				} catch (Exception e) {
				}
			}
		} else {
			System.out.println("Folder not exist in the given location:"
					+ perfFolderName);
		}

	}
}
