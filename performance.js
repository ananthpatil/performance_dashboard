var jsonData;
var dispType;

var app = angular.module("performance", ['ngTouch', 'ngAnimate', 'ui.bootstrap']);
app.controller("myCtl", ['$scope', '$http', '$log', '$filter', '$timeout', '$uibModal',
	function ($scope, $http, $log, $filter, $timeout, $uibModal) {
		$scope.displayData = function () {
			$scope.myClass = 'bodyclass';
			$scope.loading = true;
			$http.get('http://10.33.177.59:8085/displaydata').then(function (response) {
				$scope.perfdata = response.data.records;
				$scope.loading = false; $scope.myClass = '';
				$scope.perf = {}; $scope.nonfilter = true;
				$scope.filtered = false; $scope.comp = false;
				$scope.starterror = ''; $scope.enderror = '';
				$scope.dispgraph = false;
				//$scope.baseline = false;
				var ninArray = []; var errorArray = [];
				var avgArray = [];
				try {
					for (var x = 0; x < $scope.perfdata.length; x++) {
						avgArray[x] = $scope.perfdata[x].average;
						ninArray[x] = $scope.perfdata[x].nintyPer;
						errorArray[x] = $scope.perfdata[x].error;
					}

					avgArray.sort(function (a, b) { return a - b });
					ninArray.sort(function (a, b) { return a - b });
					errorArray.sort(function (a, b) { return a - b });

					var val = convertMiliToSeconds(avgArray[avgArray.length - 1]);
					$scope.avgRes = val;

					var val2 = convertMiliToSeconds(ninArray[ninArray.length - 1]);
					$scope.maxNintyRes = val2;
					$scope.maxError = parseInt(errorArray[errorArray.length - 1]);
					$timeout(function () {
						$scope.perf.average = val;
						$scope.perf.nintypercentage = val2;
						$scope.perf.errorval = angular.copy($scope.maxError);
						applyStyle();
					}, 20)
				} catch (err) {
					$log.error('Error in displayData:' + err.message);
					$scope.loading = false;
				}
			}).catch(function (err) {
				$scope.loading = false;
				$log.error('Error' + err.message);
			});
		};


		/*$scope.displayBaseline = function () {
			$http.get('http://localhost:8085/displaybaseline').then(function (response) {
				$scope.baselineData = response.data.records;
				$scope.perf = {};
				$scope.nonfilter = false; $scope.filtered = false;
				//$scope.baseline = true;
				var ninArray = []; var errorArray = [];
				var avgArray = [];
				try {

					for (var x = 0; x < $scope.baselineData.length; x++) {
						avgArray[x] = $scope.baselineData[x].average;
						ninArray[x] = $scope.baselineData[x].nintyPer;
						errorArray[x] = $scope.baselineData[x].error;
					}

					avgArray.sort(function (a, b) { return a - b });
					ninArray.sort(function (a, b) { return a - b });
					errorArray.sort(function (a, b) { return a - b });

					var val = convertMiliToSeconds(avgArray[avgArray.length - 1]);
					$scope.avgRes = val;

					var val2 = convertMiliToSeconds(ninArray[ninArray.length - 1]);
					$scope.maxNintyRes = val2;

					$scope.maxError = parseInt(errorArray[errorArray.length - 1]);
					$timeout(function () {
						$scope.perf.average = val;
						$scope.perf.nintypercentage = val2;
						$scope.perf.errorval = angular.copy($scope.maxError);
						applyStyle();
					}, 20)
				} catch (err) {
					$log.error('Error in displayData:' + err.message);
				}
			}).catch(function (err) {
				$log.error('Error' + err.message);
			});
		};*/

		function applyStyle() {
			$scope.myStyle = {
				"display": "block"
			};
		};


		$scope.availableDates = function (callback) {
			try {
				callback();
				if ($scope.startdate !== undefined) {
					$scope.starterror = '';
					var strtDatVal = $scope.startdate;
					$scope.minEndDate = $filter('date')(new Date(strtDatVal), 'yyyy-MM-dd');
					strtDatVal.setDate(strtDatVal.getDate() + 15);
					$scope.maxEndDate = $filter('date')(new Date(strtDatVal), 'yyyy-MM-dd');
				} else {
					$log.info('undefined');
				}
			} catch (err) {
				$log.error('Error in availableDates:' + err.message);
			}
		}


		$scope.clearErrorMessage = function () {
			$scope.enderror = '';
		}


		$scope.clearEndData = function () {
			$scope.enddate = '';
		}


		$scope.clearFilter = function () {
			$scope.perf.average = $scope.avgRes;
			$scope.perf.nintypercentage = $scope.maxNintyRes;
			$scope.perf.errorval = $scope.maxError;
			$scope.startdate = ''; $scope.enddate = '';
			$scope.query = {}; $scope.filtered = false;
			/*if ($scope.baseline === true)
				$scope.baseline = true;*/
			if ($scope.comp === true)
				$scope.comp = true;
			else
				$scope.nonfilter = true;
		};


		$scope.applyFilter = function () {
			try {
				var perfData;
				/*if ($scope.baseline === true)
					perfData = $scope.baselineData;*/
				if ($scope.nonfilter === true)
					perfData = $scope.perfdata;
				if ($scope.filtered === true)
					perfData = $scope.filteredJson;


				if (perfData !== undefined) {
					var filterStrtDat, filterEndDat, filterAvgRes;

					filterStrtDat = new Date(document.getElementById('startid').value);
					filterEndDat = new Date(document.getElementById('endid').value);
					filterAvgRes = $scope.perf.average;
					filterNintyRes = $scope.perf.nintypercentage;
					filterErrorVal = $scope.perf.errorval;
					var dataArray = [];
					if (perfData[0].samplerIndex === undefined)
						$scope.base = true;
					else
						$scope.base = false;

					for (var i = 0; i < perfData.length; i++) {

						var filterStatus = true;
						var counter = 0;
						if (filterStrtDat.toString() !== 'Invalid Date') {
							var exec = new Date($filter('date')(new Date(perfData[i].execDate), 'yyyy-MM-dd'));
							if (filterEndDat.toString() !== 'Invalid Date') {
								if (exec < filterStrtDat || exec > filterEndDat) {
									filterStatus = false;
									counter++;
								}
							} else {
								if (exec < filterStrtDat) {
									filterStatus = false;
									counter++;
								}
							}
						}
						var avgRes = convertMiliToSeconds(perfData[i].average);
						var nintyPerRate = convertMiliToSeconds(perfData[i].nintyPer);
						var errRate = perfData[i].error;

						if (avgRes > filterAvgRes) {
							filterStatus = false;
							counter++;
						}
						if (nintyPerRate > filterNintyRes || errRate > filterErrorVal) {
							filterStatus = false;
							counter++;
						}
						if (filterStatus && counter === 0) {
							dataArray.push({
								samplerIndex: parseInt(perfData[i].samplerIndex),
								brandName: perfData[i].brandName,
								execDate: perfData[i].execDate, sampler: perfData[i].sampler,
								average: parseInt(perfData[i].average),
								nintyPer: parseInt(perfData[i].nintyPer),
								error: parseInt(perfData[i].error)
							});
						}
					}
					$scope.nonfilter = false;
					//$scope.baseline = false;
					$scope.filtered = true;
					$scope.dispgraph = false;
					var objMap = { "records": dataArray };
					$scope.filteredJson = objMap.records;
				}
			} catch (err) {
				$log.error('Error in applyFilter:' + err.message);
			}
		};


		$scope.updateCell = function (index) {
			//$.noConflict();
			retrieveFullRowOfData(index).then(function (jsonObject) {
				$http({
					url: 'http://10.33.177.59:8085/updatedata',
					method: 'POST',
					data: jsonObject,
					headers: { 'Content-Type': 'application/json' }
				}).then(function (response) {
					openModal('Updated succesfully!');
				}, function (response) {
					openModal('Failed to update!');
					$log.info('error' + response.message);
				});
			}).catch(function (err) {
				openModal('Failed to update!');
				$log.error("Error in updateCell:" + err.message);
			});
		};



		/*$scope.compareSample = function (index) {
			//$.noConflict();
			retrieveFullRowOfData(index).then(function (jsonObject) {
				$scope.myClass = 'bodyclass';
				$scope.loading = true;
				$http({
					url: 'http://localhost:8085/compareSample',
					method: 'POST',
					data: jsonObject,
					headers: { 'Content-Type': 'application/json' }
				}).then(function (response) {
					$scope.myClass = '';
					$scope.loading = false;
				}, function (response) {
					$scope.myClass = '';
					$scope.loading = false;
					$log.info('error' + response.message);
				});
			}).catch(function (err) {
				$scope.myClass = '';
				$scope.loading = false;
				$log.error("Error in updateCell:" + err.message);
			});
		};*/

		$scope.displayoriginresult = function () {
			$scope.dispgraph = false;
			if (dispType === 'filtered')
				$scope.filtered = true;
			if (dispType === 'nonfilter')
				$scope.nonfilter = true;
		}


		$scope.displayGraph = function (index, type) {
			dispType = type;
			strtDate = document.getElementById('startid').value;
			endDate = document.getElementById('endid').value;
			if (strtDate.length === 0) {
				formArrayOfJsonData(index).then(function (jsonObject) {
					$scope.filtered = false;
					$scope.nonfilter = false;
					$scope.dispgraph = true;
					jsonData = jsonObject.records;
					$timeout(function () {
						google.charts.setOnLoadCallback(drawChartForNinty);
					}, 200)
					$timeout(function () {
						google.charts.setOnLoadCallback(drawChartForError);
					}, 200)
				});
			} else {
				strtDate = document.getElementById('startid').value;
				endDate = document.getElementById('endid').value;
				retrieveRequiredData(index).then(function (jsonObject) {
					$scope.myClass = 'bodyclass';
					$scope.loading = true;
					$http({
						url: 'http://10.33.177.59:8085/displaygraph',
						method: 'POST',
						data: jsonObject,
						headers: { 'Content-Type': 'application/json' }
					}).then(function (response) {
						$scope.filtered = false;
						$scope.nonfilter = false;
						$scope.dispgraph = true;
						$scope.loading = false; $scope.myClass = '';
						jsonData = response.data.records;
						$timeout(function () {
							google.charts.setOnLoadCallback(drawChartForNinty);
						}, 200)
						$timeout(function () {
							google.charts.setOnLoadCallback(drawChartForError);
						}, 200)
					}, function (response) {
						openModal('Failed to display graph!');
						$log.info('error' + response.message);
					});
				}).catch(function (err) {
					openModal('Failed to display graph!');
					$log.error("Error in displaying the graph:" + err.message);
				});
			}
		};


		$scope.displayDataBasedOnDate = function (colName) {
			val1 = document.getElementById('startid').value;
			val2 = document.getElementById('endid').value;
			$scope.starterror = '';
			$scope.enderror = '';
			$scope.nonfilter = false; $scope.filtered = false;
			$scope.dispgraph = false;
			//$scope.baseline = false;
			var sqlQuery = "select to_char(executiondate, 'yyyy/MM/dd HH24:MI:SS'),sampler," + colName + " from performance ";
			if (val1 === '') {
				$scope.starterror = 'Start date must be specified';
				return;
			}
			if (val2 === '') {
				$scope.enderror = 'End date must be specified';
				return;
			}
			$scope.myClass = 'bodyclass';
			$scope.loading = true;
			$http({
				url: 'http://10.33.177.59:8085/dispdataondate',
				method: 'POST',
				data: JSON.stringify({ strtDat: val1, edDate: val2, qury: sqlQuery }),
				headers: { 'Content-Type': 'application/json' }
			}).then(function (response) {
				var records = response.data.records;
				var noOfdates = response.data.noOfData;
				$scope.myClass = '';
				$scope.loading = false;
				var r;
				for (r = 0; r < records.length; r++) {
					var res = records[r].execandres;
					if (res.length < noOfdates.length) {
						var k = 0;
						for (var n = 0; n < noOfdates.length; n++) {
							try {
								if (res[k].execDate != noOfdates[n]) {
									res.splice(n, 0, { execDate: "", nintPer: "----" });
									k = n + 1;
								} else
									k++;
							} catch (error) {
								res.splice(n, 0, { execDate: "", nintPer: "----" });
							}
						}
					}
				}
				if (r === records.length) {
					$scope.displayDataOnDate = records;
					$scope.noOfData = noOfdates;
					$scope.comp = true;
					applyStyle();
				}
			}, function (response) {
				$scope.myClass = '';
				$scope.loading = false;
				$log.info('error' + response.message);
			});
		}


		function openModal(resp) {
			var modalInstance = $uibModal.open({
				templateUrl: 'ModalContent.html',
				controller: "ModalContentCtrl",
				size: '',
				resolve: {
					modalresp: function () {
						return resp;
					}
				}
			});
		}

	}]);


app.controller('ModalContentCtrl', function ($scope, $uibModalInstance, modalresp) {
	$scope.resp = modalresp;
	$scope.ok = function () {
		$uibModalInstance.close("Ok");
	}

	$scope.cancel = function () {
		$uibModalInstance.dismiss();
	}
});


function retrieveFullRowOfData(index) {
	return new Promise(function (resolve, reject) {
		jQuery(document).ready(function () {
			jQuery("#jtldata tr:nth-child(" + (index + 1) + ")").each(function (i, element) {
				var chdTd = jQuery(element).children();
				var updatedData = {
					samplerIndex: parseInt(chdTd.eq(0).text()),
					brandName: chdTd.eq(1).text(),
					execDate: chdTd.eq(2).text(),
					sampler: chdTd.eq(3).text(),
					average: parseInt(chdTd.eq(4).text()),
					nintyPer: parseInt(chdTd.eq(5).text()),
					error: parseFloat(chdTd.eq(6).text())
				};
				if (chdTd.length !== 0)
					resolve(updatedData);
				else
					reject(new Error('There are no data in table'));
			});
		});
	});
};



function retrieveRequiredData(index) {
	return new Promise(function (resolve, reject) {
		jQuery(document).ready(function () {
			jQuery("#jtldata tr:nth-child(" + (index + 1) + ")").each(function (i, element) {
				var chdTd = jQuery(element).children();
				strtDate = jQuery('#startid').val();
				endDate = jQuery('#endid').val();
				if (endDate.length === 0) {
					endDate = new Date(strtDate);
					endDate.setDate(endDate.getDate() + 15);
					var d = endDate.getDate();
					var m = endDate.getMonth();
					m += 1;
					var y = endDate.getFullYear();
					endDate = y + "-" + m + "-" + d;
				}
				var updatedData = {
					strtDat: strtDate,
					edDate: endDate,
					nintyPer: parseInt(chdTd.eq(5).text()),
					brandName: chdTd.eq(1).text(),
					sampler: chdTd.eq(3).text(),
					error: parseFloat(chdTd.eq(6).text())
				};
				if (chdTd.length !== 0)
					resolve(updatedData);
				else
					reject(new Error('There are no data in table'));
			});
		});
	});
};

function formArrayOfJsonData(index) {
	return new Promise(function (resolve, reject) {
		jQuery(document).ready(function () {
			jQuery("#jtldata tr:nth-child(" + (index + 1) + ")").each(function (i, element) {
				var chdTd = jQuery(element).children();
				var dataArray = [];
				dataArray.push({
					samplerIndex: parseInt(chdTd.eq(0).text()),
					brandName: chdTd.eq(1).text(),
					execDate: chdTd.eq(2).text(),
					sampler: chdTd.eq(3).text(),
					average: parseInt(chdTd.eq(4).text()),
					nintyPer: parseInt(chdTd.eq(5).text()),
					error: parseFloat(chdTd.eq(6).text())
				});
				var objMap = { "records": dataArray };
				if (chdTd.length !== 0)
					resolve(objMap);
				else
					reject(new Error('There are no data in table'));
			});
		});
	});
};


function convertMiliToSeconds(miliseconds) {
	var val1;
	try {
		//val1 = ((miliseconds/1000)%60);
		val1 = (miliseconds / 1000);
		var decArr = val1.toString().split(".");
		val1 = parseInt(decArr[0]) + 1;
	} catch (err) {
		$log.error('error in convertMiliToSeconds:' + err.message);
	}
	return val1;
}


function drawChartForNinty() {
	var dataTable = new google.visualization.DataTable();
	dataTable.addColumn('string', 'DateOfExecution');
	dataTable.addColumn('number', 'TimeInMs');
	dataTable.addRows(jsonData.length);
	for (var i = 0; i < jsonData.length; i++) {
		dataTable.setCell(i, 0, jsonData[i].execDate);
		dataTable.setCell(i, 1, parseInt(jsonData[i].nintyPer));
	}
	var options = {
		title: "Ninty percentage line",
		titleTextStyle: { fontSize: '14' },
		backgroundColor: { fill: 'transparent' },
		height: 300,
		fontSize: '12',
		bar: { groupWidth: "20%" },
	};
	var chart = new google.visualization.ColumnChart(document.getElementById('graphid1'));
	chart.draw(dataTable, options);
};

function drawChartForError() {
	var dataTable = new google.visualization.DataTable();
	dataTable.addColumn('string', 'DateOfExecution');
	dataTable.addColumn('number', 'Error');
	dataTable.addRows(jsonData.length);
	for (var i = 0; i < jsonData.length; i++) {
		dataTable.setCell(i, 0, jsonData[i].execDate);
		dataTable.setCell(i, 1, parseInt(jsonData[i].error));
	}
	var options = {
		title: "Error Percentage",
		titleTextStyle: { fontSize: '14' },
		backgroundColor: { fill: 'transparent' },
		height: 300,
		fontSize: '12',
		bar: { groupWidth: "20%" },
	};
	var chart = new google.visualization.ColumnChart(document.getElementById('graphid2'));
	chart.draw(dataTable, options);
};