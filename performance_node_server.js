var express = require('express');
var oracledb = require('oracledb');
var str = require('string');
var HashMap = require('hashmap');
var ArrayList = require('arraylist');
var bodyParser = require('body-parser');
var dateFormat = require('dateformat');
var Set = require('set');
var app = express();

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());
oracledb.autoCommit = true;


var server = app.listen(8085,'10.33.177.59',function () {
    var host = server.address().address
    var port = server.address().port
    console.log("Server running at host and port", host, port)
})

app.get('/', function (req, res) {
    res.writeHead(200, { 'Content-Type': 'text/html' })
    res.write("No Data Requested, so none is returned")
    res.end();
})

app.get('/performance', function (req, res) {
    res.sendFile(__dirname + "/" + "performance.html");
    setTimeout(function () {
        res.end();
    }, 1000);
})


app.post('/updatedata', function (req, res) {
    connectToDatabase(req, res).then(function (connection) {
        var samplerId = req.body.samplerIndex;
        var average = req.body.average;
        var nintyPer = req.body.nintyPer;
        var error = req.body.error;
        var sqlQuery = "update performance set AVERAGE=:avrge,NINTYPERCENTAGE=:ninPer,"
            + "ERRORRATE=:err where SAMPLERINDEX=:id";
        connection.execute(sqlQuery,
            { avrge: average, ninPer: nintyPer, err: error, id: samplerId },
            function (err, result) {
                if (err) {
                    res.writeHead(500, { 'Content-Type': 'application/json' });
                    res.end(JSON.stringify({
                        status: 500,
                        message: "Error in getting the updating the data DB",
                        detailed_message: err.message
                    })
                    );
                } else {
                    res.writeHead(200, { 'Content-Type': 'application/json' });
                    res.write(JSON.stringify({ Status: "updated" }));
                    res.end();
                }
                releaseConnection(connection);
            });
    }).catch(function (err) {
        console.log("error:" + err);
    });
});


app.post('/compareSample', function (req, res) {
    connectToDatabase(req, res).then(function (connection) {

        var sqlQuery = "select * from baseline_performance";
        connection.execute(sqlQuery, function (err, result) {
            if (err) {
                res.writeHead(500, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({
                    status: 500,
                    message: "Error in Comparing the data from baseline",
                    detailed_message: err.message
                })
                );
            } else {
                var total = result.rows.length;
                var sampIndex, bndName, execDate, samplName;
                var avg, ninPer, errVal, small;

                sampIndex = req.body.samplerIndex;
                bndName = req.body.brandName;
                execDate = req.body.execDate;
                samplName = req.body.sampler;
                avg = req.body.average;
                ninPer = req.body.nintyPer;
                errVal = req.body.error;
                if (total === 0) {
                    insertDataToBaseLine(connection, bndName, execDate, samplName, avg, ninPer,
                        errVal).then(function () {
                            res.writeHead(200, { 'Content-Type': 'application/json' });
                            res.end();
                        });
                }
                else {
                    fetchDataFromDb(connection, sampIndex, bndName, samplName).
                        then(findingSamllestResponseTime.bind(null, req)).
                        then(function (baseData) {
                            rsexecDate = baseData.execDate;
                            rsavg = baseData.average;
                            rsninPer = baseData.nintyPer;
                            rserrVal = baseData.error;
                            fetchDataFromBaselineDb(connection, bndName, samplName).
                                then(function (result1) {
                                    var totRows = result1.rows.length;
                                    if (totRows === 0) {
                                        insertDataToBaseLine(connection, bndName, rsexecDate, samplName, rsavg, rsninPer,
                                            rserrVal).then(function () {
                                                res.writeHead(200, { 'Content-Type': 'application/json' });
                                                res.end();
                                            }).catch(function (err) {
                                                console.log("Error:" + err.message);
                                            });
                                    } else {
                                        for (row = 0; row < totRows; row++) {
                                            var rowString = result1.rows[row];
                                            if (rsninPer < rowString[4]) {
                                                updateRecordsInBaselineDb(connection, rsexecDate, rsninPer, bndName, samplName).
                                                    then(function () {
                                                        res.writeHead(200, { 'Content-Type': 'application/json' });
                                                        res.end();
                                                    }).catch(function (err) {
                                                        console.log("error:" + err.message);
                                                    });
                                            }
                                        }
                                    }
                                });
                        }).catch(function (err) {
                            console.log("error:" + err.message);
                        });
                }
            }
        });
    }).catch(function (err) {
        console.log("error:" + err.message);
    });
});



app.post('/displaygraph', function (req, res) {
    connectToDatabase(req, res).then(function (connection) {
        var sampler = req.body.sampler;
        var bndname = req.body.brandName;
        var strtDat = new Date(req.body.strtDat);
        var eddate = new Date(req.body.edDate);

        strtDat = dateFormat(strtDat, 'dd-mmm-yy');
        eddate = dateFormat(eddate, 'dd-mmm-yy');

        var sqlQuery = "select * from performance where brandname=:bnd and sampler=:sampl " +
            "and executiondate>='" + strtDat + "' and executiondate <= '" + eddate + " 23.59.00'" +
            " order by executiondate desc";

        connection.execute(sqlQuery, { bnd: bndname, sampl: sampler },
            function (err, result) {
                if (err) {
                    res.writeHead(500, { 'Content-Type': 'application/json' });
                    res.end(JSON.stringify({
                        status: 500,
                        message: "Error in fetching the data from performance Table",
                        detailed_message: err.message
                    }));
                } else {
                    var total = result.rows.length;
                    var execDate, nintyPer;
                    var total = result.rows.length;
                    var dataArray = [];
                    for (i = 0; i < total; i++) {
                        var rowString = result.rows[i];
                        dataArray.push({
                            execDate: rowString[2],
                            nintyPer: rowString[7],
                            error: rowString[8]
                        });
                    }
                    var objMap = { "records": dataArray };
                    res.writeHead(200, { 'Content-Type': 'application/json' });
                    res.end(JSON.stringify(objMap));
                }
            });
    }).catch(function (err) {
        console.log("error:" + err.message);
    });
});



app.post('/dispdataondate', function (req, res) {
    connectToDatabase(req, res).then(function (connection) {
        var strtDat = new Date(req.body.strtDat);
        var eddate = new Date(req.body.edDate);
        strtDat = dateFormat(strtDat, 'dd-mmm-yy');
        eddate = dateFormat(eddate, 'dd-mmm-yy');
        var sqlQuery = req.body.qury + "where executiondate>='" + strtDat
            + "' and executiondate <= '" + eddate + " 23:59.00'";

        connection.execute(sqlQuery, function (err, result) {
            if (err) {
                res.writeHead(500, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({
                    status: 500,
                    message: "Error in Comparing the data from baseline",
                    detailed_message: err.message
                })
                );
            } else {
                var total = result.rows.length;
                var map = new HashMap();
                for (r = 0; r < total; r++) {
                    var list = new ArrayList();
                    var coldData = result.rows[r];
                    datVal = dateFormat(new Date(coldData[0]), 'yyyy/mm/dd HH:MM:ss');
                    var dateObject = { execDate: datVal, nintPer: coldData[2] };
                    list.add(dateObject);
                    if (r === 0) {
                        map.set(coldData[1], list);
                    } else {
                        var keys = map.keys();
                        if (keys.includes(coldData[1])) {
                            var strdList = map.get(coldData[1]);
                            strdList.add(list.get(0));
                            map.set(coldData[1], strdList);
                        } else {
                            map.set(coldData[1], list);
                        }
                    }
                }

                var dateList = new ArrayList();
                var promise = new Promise(function (resolve, reject) {
                    var counter = 0, val = 0; var dataArray = [];
                    map.forEach(function (value, key) {
                        (function (callback) {
                            value.sort(function (a, b) {
                                return new Date(a.execDate) - new Date(b.execDate);
                            });
                            for (var v = 0; v < value.length; v++) {
                                if (!dateList.contains(value[v].execDate)) {
                                    dateList.add(value[v].execDate);
                                }
                            }
                            callback();
                        })(caculateVariation);
                        function caculateVariation() {
                            if (value.length > 1) {
                                var curr, prev;
                                curr = value[value.length - 1].nintPer;
                                prev = value[value.length - 2].nintPer;
                                val = (prev - curr) / prev;
                                val = val * 100;
                            }
                            dataArray.push({ sampler: key, execandres: value, variation: val });
                            counter++;
                        }
                    });
                    if (counter === map.size)
                        resolve(dataArray);
                    else
                        reject(new Error('Error in crreating json format'));
                });
                promise.then(function (dataArray) {
                    dateList.sort(function (a, b) {
                        return new Date(a) - new Date(b);
                    });
                    var objMap = { "records": dataArray, "noOfData": dateList };
                    res.writeHead(200, { 'Content-Type': 'application/json' });
                    res.end(JSON.stringify(objMap));
                }).catch(function (err) {
                    console.log(err.message);
                })
            }
        });
    }).catch(function (err) {
        console.log("error:" + err.message);
    });
});


app.get('/displaydata', function (req, res) {
    //connectToDatabase(req,res,function(request,response,connection){
    connectToDatabase(req, res).then(function (connection) {
        var sqlQuery = "select samplerindex,brandname,to_char(executiondate, 'yyyy/MM/dd HH24:MI:SS')," +
            "sampler,average,max,min,nintypercentage,errorrate from performance";
	  /* var sqlQuery = "select JSON_OBJECT"+"("+"\'samplerId\' value samplerindex\*/;
        connection.execute(sqlQuery, function (err, result) {
            if (err) {
                res.writeHead(500, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({
                    status: 500,
                    message: "Error in getting the data from performance table",
                    detailed_message: err.message
                })
                );
            } else {
                var total = result.rows.length;
                var dataArray = [];
                for (i = 0; i < total; i++) {
                    var rowString = result.rows[i];
                    dataArray.push({
                        samplerIndex: rowString[0], brandName: rowString[1],
                        execDate: rowString[2], sampler: rowString[3],
                        average: rowString[4], nintyPer: rowString[7],
                        error: rowString[8]
                    });
                }
                var objMap = { "records": dataArray };
                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify(objMap));
            }
            releaseConnection(connection);
        });
    }).catch(function (err) {
        console.log("error:" + err);
    });
});


app.get('/displaybaseline', function (req, res) {
    connectToDatabase(req, res).then(function (connection) {
        var sqlQuery = "select brandname,to_char(executiondate, 'yyyy/MM/dd HH24:MI:SS')," +
            "sampler,average,nintypercentage,errorrate from baseline_performance";
        connection.execute(sqlQuery, function (err, result) {
            if (err) {
                res.writeHead(500, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify({
                    status: 500,
                    message: "Error in getting the data from baseline table",
                    detailed_message: err.message
                })
                );
            } else {
                var total = result.rows.length;
                var dataArray = [];
                for (i = 0; i < total; i++) {
                    var rowString = result.rows[i];
                    dataArray.push({
                        brandName: rowString[0],
                        execDate: rowString[1], sampler: rowString[2],
                        average: rowString[3], nintyPer: rowString[4],
                        error: rowString[5]
                    });
                }
                var objMap = { "records": dataArray };
                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify(objMap));
            }
            releaseConnection(connection);
        });
    }).catch(function (err) {
        console.log("error:" + err.message);
    });
});


var connectToDatabase = function (request, response) {
    return new Promise(function (resolve, reject) {
        var connectString = "localhost:1521/perftest";
        response.setHeader('Access-Control-Allow-Origin', '*');
        response.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        response.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
        response.setHeader('Access-Control-Allow-Credentials', true);
        oracledb.getConnection(
            {
                user: "system",
                password: "Software123",
                connectString: connectString
            },
            function (err, connection) {
                if (err) {
                    console.log('Error in connecting DB ...' + err.message);
                    response.writeHead(500, { 'Content-Type': 'application/json' });
                    response.end(JSON.stringify(
                        {
                            status: 500,
                            message: 'Error in connecting DB',
                            detailedMessage: err.msg
                        }
                    ));
                    //return;
                    reject(new Error('Error in connecting DB'));
                } else {
                    console.log('Connection recieved ; go execute');
                    resolve(connection);
                    //callback(request, response, connection);
                }
            });
    });
};


function findingSamllestResponseTime(req, dataArray) {
    return new Promise(function (resolve, reject) {
        try {
            var counter = -1;
            var ninPer = req.body.nintyPer;
            var small = ninPer;
            for (d = 0; d < dataArray.length; d++) {
                var data = dataArray[d];
                var dbNinPer = data.nintyPer;
                if (dbNinPer < small) {
                    small = dbNinPer;
                    counter = d;
                }
            }
            if (counter >= 0) {
                resolve(dataArray[counter]);
            } else {
                resolve({
                    brandName: req.body.brandName,
                    execDate: req.body.execDate,
                    sampler: req.body.sampler,
                    average: req.body.average,
                    nintyPer: ninPer,
                    error: req.body.error
                });
            }
        } catch (err) {
            reject(err);
        }
    });
}

function updateRecordsInBaselineDb(connection, execDate, ninPer, bndName, sampler) {
    var sqlQuery = "update baseline_performance set EXECUTIONDATE=:1,NINTYPERCENTAGE=:2"
        + " where BRANDNAME=:3 AND SAMPLER=:4";
    return new Promise(function (resolve, reject) {
        execDate = new Date(Date.parse(execDate));
        connection.execute(sqlQuery,
            [execDate, ninPer, bndName, sampler],
            function (err, result) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
                releaseConnection(connection);
            });
    });
}

function fetchDataFromBaselineDb(connection, brandName, samplerName) {
    return new Promise(function (resolve, reject) {
        var sqlQuery = "select brandname,to_char(executiondate, 'yyyy/MM/dd HH24:MI')," +
            "sampler,average,nintypercentage,errorrate from " +
            "baseline_performance where (brandname=:1 and sampler=:2)";
        connection.execute(sqlQuery,
            [brandName, samplerName],
            function (err, result) {
                if (err) {
                    reject(err.message);
                } else {
                    resolve(result);
                }
                //releaseConnection(connection);
            });
    });
}


function fetchDataFromDb(connection, smpIndex, brandName, samplerName) {
    return new Promise(function (resolve, reject) {
        var sqlQuery = "select samplerindex,brandname,to_char(executiondate, 'yyyy/MM/dd HH24:MI')," +
            "sampler,average,max,min,nintypercentage,errorrate from " +
            "performance where (brandname=:1 and sampler=:2) " +
            "and not samplerindex=:3";
        connection.execute(sqlQuery,
            [brandName, samplerName, smpIndex],
            function (err, result) {
                if (err) {
                    reject(new Error('Error in retrieving the data from DB'));
                } else {
                    var total = result.rows.length;
                    var dataArray = [];
                    for (i = 0; i < total; i++) {
                        var rowString = result.rows[i];
                        dataArray.push({
                            samplerIndex: rowString[0], brandName: rowString[1],
                            execDate: rowString[2], sampler: rowString[3],
                            average: rowString[4], nintyPer: rowString[7],
                            error: rowString[8]
                        });
                    }
                    resolve(dataArray);
                }
            });
    });
}


function insertDataToBaseLine(connection, bndName, execDate,
    samplName, avg, ninPer, errVal) {
    return new Promise(function (resolve, reject) {
        execDate = new Date(Date.parse(execDate));
        var sqlQuery = "Insert into baseline_performance values(:bnd, :dat, :name, :ag, :nin, :er)";
        connection.execute(sqlQuery,
            { bnd: bndName, dat: execDate, name: samplName, ag: avg, nin: ninPer, er: errVal },
            function (err, result) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
                releaseConnection(connection);
            });
    });
}


function releaseConnection(connection) {
    connection.release(function (err) {
        if (err) {
            console.error(err.message);
        }
    });
}